const { Router } = require("express");
const connectionADB = require("../models/artistsDB");
const router = Router();

router.get("/test", (req, res) => {
  res.json({ title: "hello fucking world!" });
});

router.post("/test", (req, res) => {
  console.log(req.body);
  res.send("received");
});

router.get("/dd", (req, res) => {
  connectionADB.getConnection((err, tmpConnection) => {
    if (err) {
      tmpConnection.release();
      console.log("Error in ADB connection");
    } else {
      console.log("Connected to ADB!");
      tmpConnection.query("SELECT * FROM artista", (err, rows, fields) => {
        tmpConnection.release();
        if (err) {
          console.log("Error in the query");
        } else {
          res.json(rows);
        }
      });
    }
  });
});

module.exports = router;
