import React from "react";
import { render } from "react-dom";

class App extends React.Component {
  render() {
    return (
      <div>
        <div className="btn-group">
          <button type="button" className="btn btn-primary">
            Play
          </button>
          <button type="button" className="btn btn-primary">
            Pause
          </button>
          <button type="button" className="btn btn-primary">
            Stop
          </button>
        </div>
      </div>
    );
  }
}

render(<App />, document.getElementById("app"));
